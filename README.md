# DHT11 driver for the ESP32
This is a driver for the DHT11 temperature and humidity sensor.

## Installation
Copy the contents of this repository into the `components` directory of your project directory and include the library with the header file `dht11.h`. Refer to the esp-idf documentation for alternative ways of including external components.

## Usage
All functions of this library are prefixed with `dht11_`.

Set up the connection with `dht11_setup()`. Read sensor data with one of the `dht11_read_*()` functions. Refer to `include/dht11.h` for a more detailed API description.

An external pull up resistor must be present on the data line (e. g. 5.1 kOhm). Some sensor boards have this already installed.

## Notes
* This library has been developed for learning purposes and has not been thoroughly tested.
* This library is interrupt driven, so reading the sensor won't block everything else.
* The resolution of the readings are 0.1 °C and 1 %RH.
* Minimum sampling time is 1 s.
