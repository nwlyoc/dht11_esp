#include <stdio.h>
#include "driver/gpio.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_timer.h"
#include "hal/gpio_types.h"
#include "rom/ets_sys.h"
#include "soc/gpio_num.h"
#include "freertos/FreeRTOS.h"
#include "freertos/projdefs.h"
#include "freertos/queue.h"
#include "dht11.h"

/**
 * `#define`s for signal lengths in microseconds
 *
 * Signals lengths are described in data sheet, tolerance and timout can be set as seen fit. Tests show that
 * TOLERANCE_US should be at least 12 us.
 */
#define START_SIGNAL_US                (19U * 1000)
#define START_WAIT_FOR_RESPONSE_MIN_US 20U
#define START_WAIT_FOR_RESPONSE_MAX_US 40U
#define START_RESPONSE_US              80U
#define START_TRANSMISSION_US          80U
#define BIT_START_US                   50U
#define BIT_0_US                       28U
#define BIT_1_US                       70U
#define END_TRANSMISSION_US            50U
#define TOLERANCE_US                   15U  /*!< tolerance for signal lengths (length +/- tolerance) */
#define TIMEOUT_US                     100U /*!< must be more than longest signal (80 us) + tolerance */

/**
 * `#define`s for amount of signals, maximum transmission time and minimum sample time
 */
#define TRANSMISSION_BITS        40U
#define MAX_TRANSMISSION_SIGNALS (2 + TRANSMISSION_BITS * 2 + 1)
#define MAX_TRANSMISSION_TIME_US                                                                                \
    (START_RESPONSE_US + START_TRANSMISSION_US + TRANSMISSION_BITS * (BIT_START_US + BIT_1_US + TOLERANCE_US) + \
     END_TRANSMISSION_US)
#define MIN_SAMPLE_TIME_MS       1000U

static const char *tag_s = "dht11";
static gpio_num_t  data_pin_;
volatile uint64_t  signal_timestamp_last;
QueueHandle_t      signal_received;
SemaphoreHandle_t  sensor_mtx;

static void      signal_handler(void *args);
static esp_err_t receive_raw(uint8_t signal_lengths[MAX_TRANSMISSION_SIGNALS]);
static esp_err_t process_transmission(uint8_t signal_lengths[MAX_TRANSMISSION_SIGNALS], uint8_t data_bytes[5]);

static void IRAM_ATTR signal_handler(void *args)
{
    uint64_t received = esp_timer_get_time();
    uint8_t  length = received - signal_timestamp_last; // potential risk of receiving too long signal within time limit
                                                        // that is truncated to 8 bit and becomes a valid value

    signal_timestamp_last = received;
    xQueueSendFromISR(signal_received, &length, NULL);
}

static esp_err_t receive_raw(uint8_t signal_lengths[MAX_TRANSMISSION_SIGNALS])
{
    uint8_t    signal_received_index;
    BaseType_t receive_success = pdTRUE;
    esp_err_t  ret;

    ESP_RETURN_ON_FALSE(MIN_SAMPLE_TIME_MS * 1000 < esp_timer_get_time() - signal_timestamp_last, ESP_ERR_INVALID_STATE,
                        tag_s, "sampled too early, sensor not ready");

    ESP_RETURN_ON_FALSE(pdTRUE == xSemaphoreTake(sensor_mtx, pdMS_TO_TICKS(MIN_SAMPLE_TIME_MS)), ESP_ERR_INVALID_STATE,
                        tag_s, "sensor is blocked by another reading process");

    ESP_RETURN_ON_FALSE((signal_received = xQueueCreate(MAX_TRANSMISSION_SIGNALS, sizeof(*signal_lengths))),
                        ESP_ERR_NO_MEM, tag_s, "Queue could not be created");

    // initialization of transmission
    ESP_GOTO_ON_ERROR(gpio_set_level(data_pin_, 0), error, tag_s, "could not set level");
    ets_delay_us(START_SIGNAL_US);
    ESP_GOTO_ON_ERROR(gpio_set_level(data_pin_, 1), error, tag_s, "could not set level");
    ets_delay_us((START_WAIT_FOR_RESPONSE_MIN_US + START_WAIT_FOR_RESPONSE_MAX_US) / 2);
    signal_timestamp_last = esp_timer_get_time();

    // record signal changes via interrupts on data pin (if done without interrupts, critical section would need to
    // block everything else to get signal timings right)
    ESP_GOTO_ON_ERROR(gpio_set_intr_type(data_pin_, GPIO_INTR_ANYEDGE), error, tag_s,
                      "enabling interrupts on data pin failed");
    // wait for signals and read length directly into array (much less memory needed with calculation inside ISR instead
    // of here because queue elements are only 8 bit instead of 64 bit); time out if no signal received within timeout
    // period (at least 2 ticks, because 1 tick could be a very short period (less than a signal's length) until the
    // next tick)
    for (signal_received_index = 0; signal_received_index < MAX_TRANSMISSION_SIGNALS && pdTRUE == receive_success;
         signal_received_index++) {
        receive_success = xQueueReceive(signal_received, &signal_lengths[signal_received_index],
                                        pdMS_TO_TICKS(TIMEOUT_US / 1000) >= 2 ? pdMS_TO_TICKS(TIMEOUT_US / 1000) : 2);
    }
    ESP_GOTO_ON_ERROR(gpio_set_intr_type(data_pin_, GPIO_INTR_DISABLE), error, tag_s,
                      "disabling interrupts on data pin failed");

    // check if reception of all signals was successful
    ESP_GOTO_ON_FALSE(pdTRUE == receive_success, ESP_ERR_TIMEOUT, error, tag_s,
                      "timed out; received only %u signals instead of %u", signal_received_index,
                      MAX_TRANSMISSION_SIGNALS);

    vQueueDelete(signal_received);
    ESP_RETURN_ON_FALSE(pdTRUE == xSemaphoreGive(sensor_mtx), ESP_ERR_INVALID_STATE, tag_s, "releasing sensor failed");
    return ESP_OK;

error:
    vQueueDelete(signal_received);
    ESP_RETURN_ON_FALSE(pdTRUE == xSemaphoreGive(sensor_mtx), ESP_ERR_INVALID_STATE, tag_s, "releasing sensor failed");
    return ret;
}

static esp_err_t process_transmission(uint8_t signal_lengths[MAX_TRANSMISSION_SIGNALS], uint8_t data_bytes[5])
{
    // initialization
    ESP_RETURN_ON_FALSE(signal_lengths[0] > START_RESPONSE_US - TOLERANCE_US &&
                            signal_lengths[0] < START_RESPONSE_US + TOLERANCE_US,
                        ESP_FAIL, tag_s, "response signal too short or long (expected %u us, got %u us)",
                        START_RESPONSE_US, signal_lengths[0]);
    ESP_RETURN_ON_FALSE(signal_lengths[1] > START_TRANSMISSION_US - TOLERANCE_US &&
                            signal_lengths[1] < START_TRANSMISSION_US + TOLERANCE_US,
                        ESP_FAIL, tag_s,
                        "transmission preperation signal too short or long (expected %u us, got %u us)",
                        START_TRANSMISSION_US, signal_lengths[1]);

    // bit transfer
    uint8_t index = 2;
    for (uint8_t i = 0; i < TRANSMISSION_BITS / 8; i++) {
        data_bytes[i] = 0;
        for (uint8_t j = 0; j < 8; j++) {
            ESP_RETURN_ON_FALSE(signal_lengths[index] > BIT_START_US - TOLERANCE_US &&
                                    signal_lengths[index] < BIT_START_US + TOLERANCE_US,
                                ESP_FAIL, tag_s,
                                "bit %u: transfer preparation signal too short or long (expected %u us, got %u us)",
                                (index - 2) / 2, BIT_START_US, signal_lengths[index]);
            index++;
            ESP_RETURN_ON_FALSE(
                signal_lengths[index] > BIT_0_US - TOLERANCE_US && signal_lengths[index] < BIT_1_US + TOLERANCE_US,
                ESP_FAIL, tag_s, "bit %u: transfer too short or long (expected %u us or %u us, got %u us)",
                (index - 2) / 2, BIT_0_US, BIT_1_US, signal_lengths[index]);

            if (signal_lengths[index] < BIT_0_US + TOLERANCE_US) {
                data_bytes[i] = data_bytes[i] << 1; // bit value 0 received
            } else {
                data_bytes[i] = (data_bytes[i] << 1) | 0x01; // bit value 1 received
            }
            index++;
        }
    }

    // end transmission
    ESP_RETURN_ON_FALSE(signal_lengths[MAX_TRANSMISSION_SIGNALS - 1] > END_TRANSMISSION_US - TOLERANCE_US &&
                            signal_lengths[MAX_TRANSMISSION_SIGNALS - 1] < END_TRANSMISSION_US + TOLERANCE_US,
                        ESP_FAIL, tag_s, "bit transfer end signal too short or long (expected %u us, got %u us)",
                        END_TRANSMISSION_US, signal_lengths[MAX_TRANSMISSION_SIGNALS - 1]);

    return ESP_OK;
}

esp_err_t dht11_setup(gpio_num_t data_pin, _Bool install_isr_service)
{
    data_pin_ = data_pin;
    ESP_RETURN_ON_ERROR(gpio_set_direction(data_pin_, GPIO_MODE_INPUT_OUTPUT_OD), tag_s,
                        "seting pin to INPUT_OUTPUT_OD failed");
    ESP_RETURN_ON_ERROR(gpio_pullup_dis(data_pin_), tag_s, "disabling pullup failed");
    ESP_RETURN_ON_ERROR(gpio_set_intr_type(data_pin_, GPIO_INTR_DISABLE), tag_s,
                        "disabling interrupts on data pin failed");
    ESP_RETURN_ON_ERROR(gpio_set_level(data_pin_, 1), tag_s, "could not set level");
    if (install_isr_service) {
        ESP_RETURN_ON_ERROR(gpio_install_isr_service(ESP_INTR_FLAG_EDGE | ESP_INTR_FLAG_IRAM), tag_s,
                            "could not install ISR service");
    }
    ESP_RETURN_ON_ERROR(gpio_isr_handler_add(data_pin_, signal_handler, NULL), tag_s, "could not add ISR handler");
    ESP_RETURN_ON_FALSE((sensor_mtx = xSemaphoreCreateMutex()), ESP_ERR_NO_MEM, tag_s, "mutex couldn't be created");
    // wait for maximum transmission time in case transmission has been triggered
    ets_delay_us(MAX_TRANSMISSION_TIME_US);
    signal_timestamp_last = esp_timer_get_time();
    return ESP_OK;
}

esp_err_t dht11_uninstall(_Bool uninstall_isr_service)
{
    vSemaphoreDelete(sensor_mtx);
    if (uninstall_isr_service) {
        gpio_uninstall_isr_service();
    }
    ESP_RETURN_ON_ERROR(gpio_isr_handler_remove(data_pin_), tag_s, "could not remove ISR handler");

    return ESP_OK;
}

float dht11_read_humidity(void)
{
    dht11_data_t data = dht11_read_all();
    return data.humidity;
}

float dht11_read_temperature(void)
{
    dht11_data_t data = dht11_read_all();
    return data.temperature;
}

dht11_data_t dht11_read_all(void)
{
    dht11_data_t data = {.humidity = 0.0, .temperature = 0.0};
    uint8_t      signal_lengths[2 + TRANSMISSION_BITS * 2 + 1];
    uint8_t      data_bytes[5];

    ESP_RETURN_ON_FALSE(ESP_OK == receive_raw(signal_lengths), data, tag_s, "receiving data failed");
    ESP_RETURN_ON_FALSE(ESP_OK == process_transmission(signal_lengths, data_bytes), data, tag_s,
                        "processing raw data failed");
    ESP_RETURN_ON_FALSE(data_bytes[4] == (uint8_t)(data_bytes[0] + data_bytes[1] + data_bytes[2] + data_bytes[3]), data,
                        tag_s, "checksum doesn't match received data");

    data.humidity = data_bytes[0];
    if (data_bytes[1] >= 100) {
        data.humidity += (float)data_bytes[1] / 1000;
    } else if (data_bytes[1] >= 10) {
        data.humidity += (float)data_bytes[1] / 100;
    } else {
        data.humidity += (float)data_bytes[1] / 10;
    }

    data.temperature = data_bytes[2];
    if (data_bytes[3] >= 100) {
        data.temperature += (float)data_bytes[3] / 1000;
    } else if (data_bytes[3] >= 10) {
        data.temperature += (float)data_bytes[3] / 100;
    } else {
        data.temperature += (float)data_bytes[3] / 10;
    }

    return data;
}
