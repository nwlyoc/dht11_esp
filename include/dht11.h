#pragma once

#include "esp_err.h"
#include "soc/gpio_num.h"

/**
 * Container that holds full readout from DHT11 module. Returned by `dht11_read_all()`.
 */
typedef struct {
    float humidity;    /*!< fixed point relative humidity value (%RH) */
    float temperature; /*!< fixed point temperature value (°C) */
} dht11_data_t;

/**
 * @brief set up connection to DHT11 on specified pin
 *
 * @param data_pin GPIO that is connected to DHT11
 * @param install_isr_service Needed for receiving signals from DHT11. If ISR service has already been manually
 * installed for other functionality, put `false`.
 *
 * @return
 *  - `ESP_OK`: Success
 *  - `ESP_ERR_INVALID_ARG`: GPIO error
 *  - `ESP_ERR_NO_MEM`: out of memory
 *  - other `ESP_ERR_*` errors: errors from underlying ISR related functions
 */
extern esp_err_t dht11_setup(gpio_num_t data_pin, _Bool install_isr_service);

/**
 * @brief remove DHT11 connection
 *
 * @param uninstall_isr_service If ISR service has been manually installed for other functionality or is still needed
 * otherwise, put `false`.
 *
 * @return
 *  - `ESP_OK`: Success
 *  - other `ESP_ERR_*` errors: errors from underlying ISR related functions
 */
extern esp_err_t dht11_uninstall(_Bool uninstall_isr_service);

/**
 * @brief read humidity from DHT11 module
 *
 * @return
 *  fixed point relative humdity value (%RH); value is 0 if reading failed
 */
extern float dht11_read_humidity(void);

/**
 * @brief read temperature from DHT11 module
 *
 * @return
 *  fixed point temperature value in degrees Celsius (°C); value is 0 if reading failed
 */
extern float dht11_read_temperature(void);

/**
 * @brief read humidity and temperature from DHT11 module
 *
 * @return
 *  struct that contains both humdity (%RH) and temperature value (°C); both values are 0 if reading failed
 */
extern dht11_data_t dht11_read_all(void);
